package com.company.vehicles;

import com.company.details.Engine;
import com.company.professions.Driver;

public class SportCar extends Car {
    private int maxSpeed;


    public SportCar(String carBrand, String carClass, int carWeight, Driver driver, Engine motor, int maxSpeed) {
        super(carBrand, carClass, carWeight, driver, motor);
        this.maxSpeed = maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public String toString() {
        return super.toString() + "max speed - " + maxSpeed;
    }
}
