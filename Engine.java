package com.company.details;

public class Engine {
    private String carProducer;
    private int carPower;

    public Engine(String carProducer, int carPower) {
        this.carPower = carPower;
        this.carProducer = carProducer;
    }

    public void setCarProducer(String carProducer) {
        this.carProducer = carProducer;
    }

    public void setCarPower(int carPower) {
        this.carPower = carPower;
    }

    public String getCarProducer() {
        return carProducer;
    }

    public int getCarPower() {
        return carPower;
    }

    public String toString() {
        return "car producer " + carProducer + "; car power - " + carPower;
    }

}
