package com.company.vehicles;

import com.company.details.Engine;
import com.company.professions.Driver;

public class Car {

    private String carBrand;
    private String carClass;
    private int carWeight;

    private Driver driver;
    private Engine engine;

    public Car(String carBrand, String carClass, int carWeight, Driver driver, Engine engine) {
        this.carBrand = carBrand;
        this.carClass = carClass;
        this.carWeight = carWeight;
        this.driver = driver;
        this.engine = engine;
    }

    public String getStart() {
        return "Let's go";
    }

    public String getStop() {
        return "Stop";
    }

    public String getTurnLeft() {
        return "turn left";
    }

    public String getTurnRight() {
        return "turn right";
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }

    public void setCarClass(String carClass) {
        this.carClass = carClass;
    }

    public void setWeight(int carWeight) {
        this.carWeight = carWeight;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public void setMotor(Engine motor) {
        this.engine = motor;
    }

    public String getCarBrand() {
        return carBrand;
    }

    public String getCarClass() {
        return carClass;
    }

    public int getWeight() {
        return carWeight;
    }

    public String toString() {
        return "Car brand - " + carBrand + "; car class - " + carClass + "; car weight - " + carWeight + ";" + "\n" + "driver - " + driver + ";" + "\n" + "motor - " + engine;
    }
}
