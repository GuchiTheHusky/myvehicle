package com.company.vehicles;

import com.company.details.Engine;
import com.company.professions.Driver;

public class Main {
    public static void main(String[] args) {
        Driver firstDriver = new Driver("Oleh", "male", 31, 976, 13);
        Engine firstEngine = new Engine("KIA MOTORS ", 2348);
        Car car = new Car("KIA", "B", 1800, firstDriver, firstEngine);

        Driver secondDriver = new Driver("Pavlo", "male", 25, 4556, 5);
        Engine secondEngine = new Engine("mersedes", 2500);
        Lorry lorryCar = new Lorry("mers", "A klas", 999, secondDriver, secondEngine, 750);

        Driver thirdDriver = new Driver("Bob", "male", 32, 103, 12);
        Engine thirdEngine = new Engine("Nissan Motors", 302);
        SportCar sportCar = new SportCar("Nissan", "C", 2252, thirdDriver, thirdEngine, 320);

        System.out.println(car);
        System.out.println();
        System.out.println(lorryCar);
        System.out.println();
        System.out.println(sportCar);

    }

}
