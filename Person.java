package com.company.entities;

import com.company.professions.Driver;

public class Person {
    private String fullName;
    private String gender;
    private int age;
    private int phoneNumber;

    public Person(String fullName, String gender, int age, int phoneNumber, int drivingExperience) {
        this.fullName = fullName;
        this.gender = gender;
        this.age = age;
        this.phoneNumber = phoneNumber;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFullName() {
        return fullName;
    }

    public String getGender() {
        return gender;
    }

    public int getAge() {
        return age;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public String toString() {
        return fullName + "; gender - " + gender + "; age - " + age + "; phone number - " + phoneNumber;
    }

}
