package com.company.vehicles;

import com.company.details.Engine;
import com.company.professions.Driver;

public class Lorry extends Car {

    private int carringCapacity;

    public Lorry(String carBrand, String carClass, int carWeight, Driver driver, Engine motor, int carringCapacity) {
        super(carBrand, carClass, carWeight, driver, motor);
        this.carringCapacity = carringCapacity;
    }

    public void setCarringCapacity(int carringCapacity) {
        this.carringCapacity = carringCapacity;
    }

    public int getCarringCapacity() {
        return carringCapacity;
    }

    public String toString() {
        return super.toString() + " carring capacity - " + carringCapacity;
    }

}
