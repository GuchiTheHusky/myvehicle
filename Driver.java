package com.company.professions;

import com.company.entities.Person;

public class Driver extends Person {
    private int drivingExperience;

    public Driver(String fullName, String gender, int age, int phoneNumber, int drivingExperience) {
        super(fullName, gender, age, phoneNumber, drivingExperience);
        this.drivingExperience = drivingExperience;
    }


    public void setDrivingExperience(int drivingExperience) {
        this.drivingExperience = drivingExperience;
    }

    public int getDrivingExperience() {
        return drivingExperience;
    }

    public String toString() {
        return super.toString() + "; drivingExperience - " + drivingExperience;
    }

}